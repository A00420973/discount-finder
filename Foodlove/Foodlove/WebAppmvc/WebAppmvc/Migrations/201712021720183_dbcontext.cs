namespace WebAppmvc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dbcontext : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BuyNows", "CreditCardType", c => c.String(nullable: false));
            DropColumn("dbo.BuyNows", "CreditType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BuyNows", "CreditType", c => c.String(nullable: false));
            DropColumn("dbo.BuyNows", "CreditCardType");
        }
    }
}
