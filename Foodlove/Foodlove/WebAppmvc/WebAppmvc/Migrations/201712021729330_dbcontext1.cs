namespace WebAppmvc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dbcontext1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BuyNows", "CreditCardName", c => c.String(nullable: false, maxLength: 100));
            DropColumn("dbo.BuyNows", "NameOnCreditCard");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BuyNows", "NameOnCreditCard", c => c.String(nullable: false, maxLength: 100));
            DropColumn("dbo.BuyNows", "CreditCardName");
        }
    }
}
