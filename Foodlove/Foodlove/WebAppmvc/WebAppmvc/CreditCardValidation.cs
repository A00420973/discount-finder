﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using WebAppmvc.Models;

namespace WebAppmvc
{
    public class CreditCardValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            BuyNow buynow = (BuyNow)validationContext.ObjectInstance;
            String type = buynow.creditCardType;
            if (type != null)
            {
                string regpattern;
                bool comparereg = false;
                type = buynow.creditCardType.ToString();
                string val = value.ToString();
                if (type == "Master Card")
                {
                    regpattern = "^5[1-5]\\d{14}$";
                    Regex regex = new Regex(regpattern);
                    comparereg = regex.IsMatch(val);
                }
                else if (type == "Visa")
                {
                    regpattern = "^4\\d{15}$";
                    Regex regex = new Regex(regpattern);
                    comparereg = regex.IsMatch(val);
                }
                else if (type == "American")
                {
                    regpattern = "^3[47]\\d{13}$";
                    Regex regex = new Regex(regpattern);
                    comparereg = regex.IsMatch(val);
                }

                if (comparereg == false)
                {
                    return new ValidationResult("Invalid credit card number");
                }
            }
            return ValidationResult.Success;
        }
    }
}

