﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using WebAppmvc.Models;
using System.Globalization;

namespace WebAppmvc
{
    public class ValidateDateRange : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

            //if (value != null)
            //{
            //    // your validation logic
            //    DateTime dtV = (DateTime)value;

            //    if (dtV >= Convert.ToDateTime("01/10/2008") && dtV <= Convert.ToDateTime("01/12/2008"))
            //    {
            //        return ValidationResult.Success;
            //    }
            //    else
            //    {
            //        return new ValidationResult("Date is not in given range.");
            //    }
            //}
            //else
            //{
            //    return new ValidationResult("" + validationContext.DisplayName + " is required");
            //}
            if (value != null)
            {
                string strValue = value.ToString();
                var dateTime = DateTime.Parse(strValue);
                string pattern = "^0[1-9]|1[0-2]/d{4}$";
                Match match = Regex.Match(strValue, pattern);
                if (match.Success)
                {

                    //validation logic
                    if (dateTime >= Convert.ToDateTime("01/2016") && dateTime <= Convert.ToDateTime("12/2031"))
                    {

                        return ValidationResult.Success;
                    }
                    else
                    {
                        return new ValidationResult("Date is not in given range.");
                    }
                }
                else
                {
                    return new ValidationResult("" + validationContext.DisplayName + " is not in proper format");
                }
            }      
          
            else
            {
                return new ValidationResult("" + validationContext.DisplayName + " is required");
            }

        }
    }

    }
