﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppmvc.Models
{
    public class RestaurantDiscount
    {
        public int Id { get; set; }
        public String RestaurantName { get; set; }
        public String Item { get; set; }
        public int Discount { get; set; }
        public int RestaurantId { get; set; }
    }
}