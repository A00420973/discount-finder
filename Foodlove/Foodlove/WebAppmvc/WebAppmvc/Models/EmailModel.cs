﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAppmvc.Models
{
    public class EmailModel
    {

        [Key]
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        // public HttpPostedFileBase Attachment { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}