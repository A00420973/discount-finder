﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppmvc.Models
{
    public class Restaurant
    {

        public int Id { get; set; }
        public String Name { get; set; }
        public String City { get; set; }
        public String Street { get; set; }
        public String Province { get; set; }
        public ICollection<RestaurantDiscount> Discount { get; set; }

    }
}