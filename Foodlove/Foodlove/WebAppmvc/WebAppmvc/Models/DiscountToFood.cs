﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace WebAppmvc.Models
{
    public class DiscountToFood : DbContext
    {
        public DbSet<Restaurant> Restaurant { get; set; }
        public DbSet<RestaurantDiscount> RestaurantDiscount { get; set; }
        public DbSet<BuyNow> BuyNow { get; set; }
        // public DbSet<CustomersLogin> CustomersLogin { get; set; }
        public DbSet<LoginCustomer> LoginCustomer { get; set; }

        public System.Data.Entity.DbSet<WebAppmvc.Models.Retaurantlistview> Retaurantlistviews { get; set; }

       // public System.Data.Entity.DbSet<WebAppmvc.Models.BuyNownew> BuyNownews { get; set; }
    }
}