﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAppmvc.Models
{
    public partial class BuyNow
    {

        //[Required(ErrorMessage = "Credit type should be selected")]
        //[DisplayName("Credit Card Type")]
        //public String CreditType { get; set; }

        [Required(ErrorMessage = "Credit type should be selected")]
        [DisplayName("Credit Card Type")]
        public String creditCardType { get; set; }


        [Required(ErrorMessage = "Credit card number is required")]
        [CreditCardValidation]
        [DisplayName("Credit Card Number")]
        public string creditCardNumber { get; set; }

        [Required(ErrorMessage = "Credit card name is required")]
        [StringLength(100)]
        [DisplayName("Name on Card")]
        [RegularExpression("[a-zA-Z]+", ErrorMessage = "Invalid Name")]
        public String creditCardName { get; set; }
    
     //   [Required(ErrorMessage = "Credit card expiry date should not be null")]
        [ValidateDateRange]
        [DisplayName("Credit Card Expiry Date(MM/YYYY)")]
     //   [RegularExpression("^0[1-9]|1[0-2]/d{4}$", ErrorMessage = "Date should be in MM/YYYY format")]
        public DateTime expiryDate { get; set; }

        [Key]
        public int creditId { get; set; }
    }
}