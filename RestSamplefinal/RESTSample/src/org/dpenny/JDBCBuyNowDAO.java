package org.dpenny;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
 

public class JDBCBuyNowDAO implements BuyNowDAO{
 
    Connection connection = null;   
    
    public Connection getConnection(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            if(connection == null)
                connection = DriverManager.getConnection("jdbc:mysql://localhost/restaurant?user=root&password=fatima");
 
        } catch (ClassNotFoundException e) {
 
            e.printStackTrace();
             
        } catch (SQLException e) {
             
            e.printStackTrace();
             
        }
        return connection;
    }

    public void closeConnection(){
        try {
              if (connection != null) {
                  connection.close();
              }
            } catch (Exception e) { 
                //do nothing
            }
    } 
    
	@Override
	public  BuyNow addPayment(BuyNow buynow) {
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement("INSERT INTO restaurant.buynow (CreditType,CreditCardNumber,CreditCardName) VALUES (?,?,?)");
         //   preparedStatement.setInt(1,  buynow.getCreditID());
            preparedStatement.setString(1,  buynow.getCreditCardType());
            preparedStatement.setString(2,  buynow.getCreditCardNumber());
            preparedStatement.setString(3,  buynow.getCreditCardName());
        //    preparedStatement.setString(5,  buynow.getExpirydate());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            System.out.println("Successfully inserted");
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return buynow;
         
    }
 

	
}