package org.dpenny;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import java.util.List;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/Restaurant")
public class DiscountController {
    @GET
    @Path("/Discount")
    @Produces({ MediaType.APPLICATION_JSON})
    public List<Discount> getRestaurantDiscount_JSON() {
    	DiscountDAO dao = new JDBCDiscountDAO();
        List<Discount> listOfAllDiscounts = dao.getAllDiscounts();
        return listOfAllDiscounts;
    }
    
}