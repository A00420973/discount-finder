package org.dpenny;

import java.util.List;

public interface RestaurantDAO {

	Restaurant getRestaurant(int id);

	Restaurant addRestaurant(Restaurant restaurant);

	Restaurant updateRestaurant(Restaurant restaurant);

	void deleteRestaurant(int id);

	List<Restaurant> getAllRestaurants();



}