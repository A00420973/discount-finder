package org.dpenny;


public class Restaurant {

	private int Id;
	private String Name;
	private String City;
	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getProvince() {
		return Province;
	}

	public void setProvince(String province) {
		Province = province;
	}

	public String getStreet() {
		return Street;
	}

	public void setStreet(String street) {
		Street = street;
	}

	private String Province;
	private String Street;

	
	public Restaurant( ) {

	}	
	
	public Restaurant( int Id, String Name,String City,String Province,String Street) {
		this.Id=Id;
		this.Name = Name;
		this.City=City;
		this.Province=Province;
		this.Street=Street;
	}
	
		
}
