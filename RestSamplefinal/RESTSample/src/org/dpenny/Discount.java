package org.dpenny;


public class Discount {

	private int Id;
	private String RestaurantName;
	private String Item;
	private String Discount;
	private int RestaurantId;
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getRestaurantName() {
		return RestaurantName;
	}
	public void setRestaurantName(String restaurantName) {
		RestaurantName = restaurantName;
	}
	public String getItem() {
		return Item;
	}
	public void setItem(String item) {
		Item = item;
	}
	public String getDiscount() {
		return Discount;
	}
	public void setDiscount(String discount) {
		Discount = discount;
	}
	public int getRestaurantId() {
		return RestaurantId;
	}
	public void setRestaurantId(int restaurantId) {
		RestaurantId = restaurantId;
	}
	
	public Discount( ) {

	}	
	
	public Discount( int Id, String RestaurantName,String Item,String Discount,int RestaurantId) {
		this.Id=Id;
		this.RestaurantName = RestaurantName;
		this.Item=Item;
		this.Discount=Discount;
		this.RestaurantId=RestaurantId;
	}
	
}
