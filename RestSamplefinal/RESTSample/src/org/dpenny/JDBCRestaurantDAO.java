package org.dpenny;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
 

public class JDBCRestaurantDAO implements RestaurantDAO{
 
    Connection connection = null;   
    
    public Connection getConnection(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            if(connection == null)
                connection = DriverManager.getConnection("jdbc:mysql://localhost/restaurant?user=root&password=fatima");
        } catch (ClassNotFoundException e) {
 
            e.printStackTrace();
             
        } catch (SQLException e) {
             
            e.printStackTrace();
             
        }
        return connection;
    }

    public void closeConnection(){
        try {
              if (connection != null) {
                  connection.close();
              }
            } catch (Exception e) { 
                //do nothing
            }
    } 
    
	@Override
	public Restaurant addRestaurant(Restaurant restaurant) {
  /*      try {
            PreparedStatement preparedStatement = getConnection().prepareStatement("INSERT INTO mcda5510dan.customer (id ,firstname) VALUES (NULL , ?)");
            preparedStatement.setString(1,  customer.getFirstName());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/
        return null;
         
    }
 

	@Override   
    public List<Restaurant> getAllRestaurants() {
        List<Restaurant> persons = new LinkedList<Restaurant>();
         try {
                Statement statement = getConnection().createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT * FROM restaurant.restaurant");
                 
                Restaurant restaurant = null;
                while(resultSet.next()){
                	restaurant = new Restaurant();
                	restaurant.setId(Integer.parseInt(resultSet.getString("id")));
                	restaurant.setName(resultSet.getString("Name"));
                	restaurant.setCity(resultSet.getString("City"));
                	restaurant.setStreet(resultSet.getString("Street"));
                	restaurant.setProvince(resultSet.getString("Province"));
                    persons.add(restaurant);
                }
                resultSet.close();
                statement.close();
                 
            } catch (SQLException e) {
                e.printStackTrace();
            }
            System.out.println(persons);
            return persons;
    }
     

	@Override
	public Restaurant getRestaurant(int id) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public Restaurant updateRestaurant(Restaurant restaurant) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteRestaurant(int id) {
		// TODO Auto-generated method stub
		
	}

}