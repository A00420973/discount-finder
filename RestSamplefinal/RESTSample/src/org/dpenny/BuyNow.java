package org.dpenny;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;
public class BuyNow {
	//private int CreditID;
	private String CreditType;
	private String CreditCardNumber;
	private String CreditCardType;
	private String CreditCardName;
  //  private String Expirydate;
    
    public BuyNow( ) {

	}	
	
	public BuyNow(String CreditType,String CreditCardNumber,String CreditCardName) {
	//	this.CreditID=CreditID;
		this.CreditType = CreditType;
		this.CreditCardNumber=CreditCardNumber;
		this.CreditCardName=CreditCardName;
	//	this.Expirydate=Expirydate;
	}

/*	public int getCreditID() {
		return CreditID;
	}

	public void setCreditID(int creditID) {
		CreditID = creditID;
	}
*/
	@JsonGetter("CreditType")
	public String getCreditType() {
		return CreditType;
	}
	@JsonSetter("CreditCardNumber")
	public void setCreditType(String CreditType) {
		this.CreditType = CreditType;
	}
	@JsonGetter("CreditCardNumber")
	public String getCreditCardNumber() {
		return CreditCardNumber;
	}
	@JsonSetter("CreditCardNumber")
	public void setCreditCardNumber(String CreditCardNumber) {
		this.CreditCardNumber = CreditCardNumber;
	}
	@JsonGetter("CreditCardName")
	public String getCreditCardName() {
		return CreditCardName;
	}
	@JsonSetter("CreditCardName")
	public void setCreditCardName(String CreditCardName) {
		this.CreditCardName = CreditCardName;
	}
	@JsonGetter("CreditCardType")
	public String getCreditCardType() {
		return CreditCardType;
	}
	@JsonSetter("CreditCardType")
	public void setCreditCardType(String CreditCardType) {
		this.CreditCardType = CreditCardType;
	}

/*	public String getExpirydate() {
		return Expirydate;
	}

	public void setExpirydate(String expirydate) {
		Expirydate = expirydate;
	}*/
}
