package org.dpenny;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
 

public class JDBCDiscountDAO implements DiscountDAO{
 
    Connection connection = null;   
    
    public Connection getConnection(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            if(connection == null)
                connection = DriverManager.getConnection("jdbc:mysql://localhost/restaurant?user=root&password=fatima");
 
        } catch (ClassNotFoundException e) {
 
            e.printStackTrace();
             
        } catch (SQLException e) {
             
            e.printStackTrace();
             
        }
        return connection;
    }

    public void closeConnection(){
        try {
              if (connection != null) {
                  connection.close();
              }
            } catch (Exception e) { 
                //do nothing
            }
    } 
    
    @Override   
    public List<Discount> getAllDiscounts() {
        List<Discount> discounts = new LinkedList<Discount>();
         try {
                Statement statement = getConnection().createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT * FROM restaurant.restaurantdiscount");
                 
                Discount discount = null;
                while(resultSet.next()){
                	discount = new Discount();
                	discount.setId(Integer.parseInt(resultSet.getString("Id")));
                	discount.setRestaurantName(resultSet.getString("RestaurantName"));
                	discount.setItem(resultSet.getString("Item"));
                	discount.setDiscount(resultSet.getString("Discount"));
                	discount.setRestaurantId(Integer.parseInt(resultSet.getString("RestaurantId")));
                	discounts.add(discount);
                }
                resultSet.close();
                statement.close();
                 
            } catch (SQLException e) {
                e.printStackTrace();
            }
            System.out.println(discounts);
            return discounts;
    }
     

	
}