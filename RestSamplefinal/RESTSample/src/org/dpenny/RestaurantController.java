package org.dpenny;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/Restaurant")
public class RestaurantController {
    @GET
    @Produces({ MediaType.APPLICATION_JSON})
    public List<Restaurant> getRestaurant_JSON() {
    	RestaurantDAO dao = new JDBCRestaurantDAO();
        List<Restaurant> listOfRestaurants = dao.getAllRestaurants();
        return listOfRestaurants;
    }
 
    @GET
    @Path("/{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Restaurant getRestaurant(@PathParam("id") int id) {
    	RestaurantDAO dao = new JDBCRestaurantDAO();
        return dao.getRestaurant(id);
    }
 
    // URI:
    // /contextPath/servletPath/Customers
    @POST
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Restaurant addRestaurant(Restaurant restaurant) {
    RestaurantDAO dao = new JDBCRestaurantDAO();
        return dao.addRestaurant(restaurant);
    }
 
    // URI:
    // /contextPath/servletPath/Customers
    @PUT
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Restaurant updateRestaurant(Restaurant restaurant) {
        RestaurantDAO dao = new JDBCRestaurantDAO();
        return dao.updateRestaurant(restaurant);
    }
 
    @DELETE
    @Path("/{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public void deleteRestaurant(@PathParam("id") int id) {
        RestaurantDAO dao = new JDBCRestaurantDAO();
    	dao.deleteRestaurant(id);
    }	
	
	
}
