namespace WebAppmvc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate700 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BuyNows",
                c => new
                    {
                        CreditID = c.Int(nullable: false, identity: true),
                        CreditCardNumber = c.String(nullable: false),
                        CreditType = c.String(nullable: false),
                        NameOnCreditCard = c.String(nullable: false, maxLength: 100),
                        ExpiryDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.CreditID);
            
            CreateTable(
                "dbo.LoginCustomers",
                c => new
                    {
                        CustomerId = c.Int(nullable: false, identity: true),
                        CustomerName = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.CustomerId);
            
            CreateTable(
                "dbo.Restaurants",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        City = c.String(),
                        Street = c.String(),
                        Province = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RestaurantDiscounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RestaurantName = c.String(),
                        Item = c.String(),
                        Discount = c.Int(nullable: false),
                        RestaurantId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Restaurants", t => t.RestaurantId, cascadeDelete: true)
                .Index(t => t.RestaurantId);
            
            CreateTable(
                "dbo.Retaurantlistviews",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        City = c.String(),
                        Item = c.String(),
                        Discount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RestaurantDiscounts", "RestaurantId", "dbo.Restaurants");
            DropIndex("dbo.RestaurantDiscounts", new[] { "RestaurantId" });
            DropTable("dbo.Retaurantlistviews");
            DropTable("dbo.RestaurantDiscounts");
            DropTable("dbo.Restaurants");
            DropTable("dbo.LoginCustomers");
            DropTable("dbo.BuyNows");
        }
    }
}
