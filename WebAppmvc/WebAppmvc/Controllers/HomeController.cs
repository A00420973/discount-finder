﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAppmvc.Models;
using System.Web.Mvc;
using System.Data.Entity;
using System.Web.Helpers;
using System.Threading.Tasks;
using System.Net.Mail;



namespace WebAppmvc.Controllers
{
    public class HomeController : Controller
    {

        DiscountToFood _db = new DiscountToFood();
        public ActionResult Index(string option, string search)
        {
            var model = _db.Restaurant.ToList();
            // return View(model);


            //if a user choose the radio button option as Subject  
            if (option == "Name")
            {
                //Index action method will return a view with a restaurant records based on what a user specify the value in textbox  
                return View(_db.Restaurant.Where(x => x.Name == search || search == null).ToList());
            }

            else
            {
                return View(_db.Restaurant.Where(x => x.Street.StartsWith(search) || search == null).ToList());
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Details(int id)
        {

            var model1 = (from p in _db.Restaurant
                          join f in _db.RestaurantDiscount
                          on p.Id equals f.RestaurantId
                          where f.RestaurantId == id
                          select new
                          {
                              Id = p.Id,
                              Name = p.Name,
                              City = p.City,
                              Item = f.Item,
                              Discount = f.Discount
                          }).ToList()
          .Select(x => new Retaurantlistview()
          {
              Id = x.Id,
              Name = x.Name,
              City = x.City,
              Item = x.Item,
              Discount = x.Discount
          });

            return View(model1);
            //  return Redirect("~/Discount/Index");
        }


        public ActionResult BuyNow()
        {
            var model = _db.BuyNow.ToList();
            return View();


        }

        [HttpPost]
        public ActionResult BuyNow(BuyNow BuyNow)
        {

            if(ModelState.IsValid == false)
            {
                return View(BuyNow);
            }

            return RedirectToAction("PaymentDone", "Home");
           
        }
  
        public ActionResult Login()
        {
            var model = _db.LoginCustomer.ToList();
            return View();

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginCustomer objUser)
        {

            var model = _db.LoginCustomer.ToList();
            if (ModelState.IsValid)
            {
                using (DiscountToFood db = new DiscountToFood())
                {
                    var obj = db.LoginCustomer.Where(a => a.CustomerName.Equals(objUser.CustomerName) && a.Password.Equals(objUser.Password)).FirstOrDefault();
                    if (obj != null)
                    {
                        //Session["UserID"] = obj.UserId.ToString();
                        //Session["UserName"] = obj.UserName.ToString();
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return RedirectToAction("ErrorLogin");
                    }

                }
            }
            return View(objUser);
        }

        public ActionResult ErrorLogin()
        {

            return View();

        }


        public ActionResult PaymentDone()
        {

            return View();

        }


        public ActionResult SendEmail()
        {
            ViewBag.Message = "Payment done!!";

            return View();
        }


  
        [HttpPost]
        public ActionResult SendEmail(EmailModel model)
        {
            using (MailMessage mm = new MailMessage(model.Email, model.To))
            {
                mm.Subject = model.Subject;
                mm.Body = model.Body;
            
                mm.IsBodyHtml = false;
                using (SmtpClient smtp = new SmtpClient())
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential(model.Email, model.Password);
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                    ViewBag.Message = "Email sent.";
                }
            }

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (_db != null)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);

        }

    }
}