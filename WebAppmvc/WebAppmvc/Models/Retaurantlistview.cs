﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppmvc.Models
{
    public class Retaurantlistview
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String City { get; set; }
        public String Item { get; set; }
        public int Discount { get; set; }
    }
}