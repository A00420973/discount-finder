﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAppmvc.Models
{
    public class LoginCustomer
    {
        [Key]
        public int CustomerId { get; set; }
        public string  CustomerName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}